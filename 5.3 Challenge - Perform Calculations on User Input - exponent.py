base = float(input("Enter the base: "))
exponent = int(input("Enter an exponent: "))

result = base ** exponent

print(f"{base} to the power of {exponent} = {result}")
