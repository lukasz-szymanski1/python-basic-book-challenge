import easygui as gui
from PyPDF2 import PdfFileReader, PdfFileWriter


input_pdf = gui.fileopenbox(
    title="Select a PDF file...",
    default="*.pdf"
    )

if input_pdf is None:
    exit()


input_file = PdfFileReader(input_pdf)
total_pages = input_file.getNumPages()

page_start = gui.enterbox(
    title="Select number of first page",
    msg="Type a page number"
    )

if page_start is None:
    exit()

while (
    not page_start.isdigit()
    or page_start == "0"
    or int(page_start) > total_pages
):
    gui.msgbox(
        title="Error",
        msg="Type a correct number"
        )
    page_start = gui.enterbox(
    title="Select number of first page",
    msg="Type a page number"
        )
    if page_start is None:
        exit()

page_end = gui.enterbox(
    title="Select number of last page",
    msg="Type a page number"
    )

if page_end is None:
    exit()

while (
    not page_end.isdigit()
    or page_end == "0"
    or int(page_end) > total_pages
    or int(page_end) < int(page_start)
):
    gui.msgbox(
        title="Error",
        msg="Type a correct number"
        )
    page_end = gui.enterbox(
    title="Select number of last page",
    msg="Type a page number"
    )
    if page_end is None:
        exit()

output_file = gui.filesavebox(
    title="Save the PDF...",
    default="*.pdf"
    )

if output_file is None :
    exit()

while input_file == output_file:
    gui.msgbox(
        title="Error",
        msg="You can't overwrite original file!, Please choose another..."
        )
    output_file = gui.filesavebox(
    title="Save the PDF...",
    default="*.pdf"
    )
    if output_file is None:
        exit()

output_PDF = PdfFileWriter()

for page_num in range(int(page_start) - 1, int(page_end)):
    page = input_file.getPage(page_num)
    output_PDF.addPage(page)

with open(output_file, "wb") as output_file:
    output_PDF.write(output_file)
