import tkinter as tk
import random

window = tk.Tk()
window.title("Make your own poem!")

header_frame = tk.Frame()
header_label = tk.Label(master=header_frame)
header_label["text"] = "Enter your words, separated by commas."
header_label.pack()
header_frame.pack(padx=5, pady=5)

input_frame = tk.Frame()

label_noun = tk.Label(input_frame, text="Nouns:")
label_verb = tk.Label(input_frame, text="Verbs:")
label_adj = tk.Label(input_frame, text="Adjectives:")
label_prep = tk.Label(input_frame, text="Prepositions:")
label_adv = tk.Label(input_frame, text="Adverbs:")

entry_noun = tk.Entry(input_frame, width=80)
entry_verb = tk.Entry(input_frame, width=80)
entry_adj = tk.Entry(input_frame, width=80)
entry_prep = tk.Entry(input_frame, width=80)
entry_adv = tk.Entry(input_frame, width=80)

label_noun.grid(row=2, column=1, sticky=tk.E)
entry_noun.grid(row=2, column=2)
label_verb.grid(row=3, column=1, sticky=tk.E)
entry_verb.grid(row=3, column=2)
label_adj.grid(row=4, column=1, sticky=tk.E)
entry_adj.grid(row=4, column=2)
label_prep.grid(row=5, column=1, sticky=tk.E)
entry_prep.grid(row=5, column=2)
label_adv.grid(row=6, column=1, sticky=tk.E)
entry_adv.grid(row=6, column=2)
input_frame.pack(padx=5, pady=5)

generate_frame = tk.Frame(master=window)
generate_frame.pack(pady=10)
generate_button = tk.Button(generate_frame, text="Generate")
generate_button.pack()

result_frame = tk.Frame(master=window)
result_frame["relief"] = tk.GROOVE
result_frame["borderwidth"] = 5
result_label = tk.Label(master=result_frame)
result_label["text"] = "Your poem:"
result_label.pack(pady=10)
result_poem = tk.Label(result_frame)
result_poem["text"] = "Press the 'Generate' button to display your poem."
result_poem.pack(padx=5)
save_button = tk.Button(result_frame, text="Save to file")
save_button.pack(pady=10)
result_frame.pack(fill=tk.X, padx=5, pady=5)


def are_unique(word_list):
    unique_words = []
    for word in word_list:
        if word not in unique_words:
            unique_words.append(word)
    return len(word_list) == len(unique_words)


def generate_poem():
    noun = entry_noun.get().split(",")
    verb = entry_verb.get().split(",")
    adjective = entry_adj.get().split(",")
    adverb = entry_adv.get().split(",")
    preposition = entry_prep.get().split(",")

    if not (
            are_unique(noun)
            and are_unique(verb)
            and are_unique(adjective)
            and are_unique(adverb)
            and are_unique(preposition)
    ):
        result_poem["text"] = "Please do not enter duplicate words."
        return

    if (
            len(noun) < 3
            or len(verb) < 3
            or len(adjective) < 3
            or len(preposition) < 2
            or len(adverb) < 1
    ):
        result_poem["text"] = (
            "Error, three nouns, three verbs, three adjectives, two prepositions and an adverb!"
        )
        return

    noun1 = random.choice(noun)
    noun2 = random.choice(noun)
    noun3 = random.choice(noun)

    while noun1 == noun2:
        noun2 = random.choice(noun)
    while noun1 == noun3 or noun2 == noun3:
        noun3 = random.choice(noun)

    verb1 = random.choice(verb)
    verb2 = random.choice(verb)
    verb3 = random.choice(verb)

    while verb1 == verb2:
        verb2 = random.choice(verb)
    while verb1 == verb3 or verb2 == verb3:
        verb3 = random.choice(verb)

    adj1 = random.choice(adjective)
    adj2 = random.choice(adjective)
    adj3 = random.choice(adjective)
    while adj1 == adj2:
        adj2 = random.choice(adjective)
    while adj1 == adj3 or adj2 == adj3:
        adj3 = random.choice(adjective)

    prep1 = random.choice(preposition)
    prep2 = random.choice(preposition)
    while prep1 == prep2:
        prep2 = random.choice(preposition)

    adv1 = random.choice(adverb)

    if adj1[0] in "aeiou":
        article = "An"
    else:
        article = "A"

    poem = f"{article} {adj1} {noun1}\n\n"
    poem = (
            poem + f"{article} {adj1} {noun1} {verb1} {prep1} the {adj2} {noun2}\n"
    )
    poem = poem + f"{adv1}, the {noun1} {verb2}\n"
    poem = poem + f"the {noun2} {verb3} {prep2} a {adj3} {noun3}"

    result_poem["text"] = poem


def save_poem():
    type_list = [("text files", "*txt")]
    file_name = filedialog.asksaveasfilename(
        filetypes=type_list, defaultextension="*.txt"
    )

    if file_name != "":
        with open(file_name, "w") as output_file:
            output_file.writelines(results_poem["text"])


generate_button["command"] = generate_poem
save_button["command"] = save_poem

window.mainloop()
