class Animal:

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def eat(self):
        print(f"{self.name} is eating.")

    def sleep(self):
        print(f"{self.name} is sleeping.")
        

class Cow(Animal):
    def __init__(self, name, age, milk_production):
        super().__init__(name, age)
        self.milk_production = milk_production

    def moo(self):
        print(f"{self.name} says moo.")


class Chicken(Animal):
    def __init__(self, name, age, egg_production):
        super().__init__(name, age)
        self.egg_production = egg_production

    def cluck(self):
        print(f"{self.name} says cluck.")


class Sheep(Animal):
    def __init__(self, name, age, wool_amount):
        super().__init__(name, age)
        self.wool_amount = wool_amount

    def baa(self):
        print(f"{self.name} says baa.")
            


cow = Cow("Bessie", 5, 20)
chicken = Chicken("Clara", 2, 150)
sheep = Sheep("Shaun", 3, 10)

              
cow.eat()         
cow.sleep()      
cow.moo()        
print(cow.milk_production)  

chicken.eat()     
chicken.sleep()   
chicken.cluck()   
print(chicken.egg_production)  

sheep.eat()      
sheep.sleep()     
sheep.baa()       
print(sheep.wool_amount)  




        
