from pathlib import Path
from PyPDF2 import PdfFileReader, PdfFileWriter 
# using PyPDF2 v 1.26.0

class PdfFileSplitter:
    def __init__(self, pdf_path):
        self.pdf_reader = PdfFileReader(pdf_path)
        self.writer1 = None
        self.writer2 = None

    def split(self, breakpoint):
        self.writer1 = PdfFileWriter()
        self.writer2 = PdfFileWriter()

        for page in self.pdf_reader.pages[:breakpoint]:
            self.writer1.addPage(page)

        for page in self.pdf_reader.pages[breakpoint:]:
            self.writer2.addPage(page)


    def write(self, filename):
        with Path(filename + "_1.pdf").open(mode="wb") as output_file:
            self.writer1.write(output_file)

        with Path(filename + "_2.pdf").open(mode="wb") as output_file:
            self.writer2.write(output_file)


pdf_splitter = PdfFileSplitter(r"C:\Za Rączke\practice_files\Pride_and_Prejudice.pdf")
pdf_splitter.split(breakpoint=150)
pdf_splitter.write("pride_split")